@extends('layout.master')
@section('judul')
Buat Account Baru
@endsection
@section('content')
<h3>Sign Up Form</h3>
<form action="/kirim" method="post">
    @csrf
    <label>First Name :</label>
    <br><br>
    <input type="text" name="firstname">
    <br><br>
    <label>Last Name :</label>
    <br><br>
    <input type="text" name="lastname">
    <br><br>
    <label>Gender</label>
    <br><br>
    <input type="radio" id="male" name="gender" value="male">
    <label for="male">Male</label>
    <br>
    <input type="radio" id="female" name="gender" value="female">
    <label for="female">Female</label>
    <br><br>
    <label>Nationality</label>
    <br><br>
    <select name="nationality">
        <option value="Indonesia">Indonesia</option>
        <option value="America">Amerika</option>
        <option value="Russia">Russia</option>
    </select>
        <br><br>
        <label>Language Spoken</label>
        <br><br>
        <input type="checkbox" id="language1" name="language1" value="Indonesia">
        <label for="language1">Indonesia</label>
        <br>
        <input type="checkbox" id="language2" name="language2" value="English">
        <label for="language2">English</label>
        <br>
        <input type="checkbox" id="language3" name="language3" value="Other">
        <label for="language3">Other</label>
    <br><br>
    <label>Bio</label>
    <br><br>
    <textarea name="bio" rows="10" cols="30"></textarea>
    <br><br>
    <input type="submit" value="Sign Up">
</form>
@endsection