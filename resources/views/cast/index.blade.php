@extends('layout.master')
@section('judul')
Daftar Pemain Baru
@endsection
@section('content')
<a href="/cast/create" class="btn btn-primary my-2">Tambah Pemain</a>
<table class="table table-striped">
    <thead>
        <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
            <tr>
                <th scope="row">{{ $key + 1 }}</th>
                <td>{{ $item->nama }}</td>
                <td>{{ $item->umur }}</td>
                <td>{{ $item->bio }}</td>
                <td>
                    <form action="/cast/{{ $item->id }}" method="post">
                        @csrf
                        @method("delete")
                        <a href="/cast/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/cast/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
            </tr>    
        @empty
            <h1>Tidak Ada Pemain </h1>
        @endforelse
    </tbody>
</table>
@endsection